import com.GrandMA.NumberToString.exception.MoneyFormatException;
import com.GrandMA.NumberToString.service.NumberService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class NumberServiceTest {

    private NumberService service = new NumberService();

    @Test
    public void testGetDigitAsStringValueWithCorrectValue() {
        String exceptedResponse =
                "one million two hundred thirty-four thousand five hundred sixty-seven dollars eighty-nine cents";

        String response = service.convert("1234567.89");

        Assert.assertEquals(exceptedResponse, response);
    }

    @Test
    public void testGetDigitAsStringValueWithNoNCorrectValue() {
        MoneyFormatException exception = assertThrows(MoneyFormatException.class, () -> {
            service.convert("SQSS");
        });

        String expectedMessage = "SQSS";
        String actualMessage = exception.getMessage();

        Assert.assertTrue(actualMessage.contains(expectedMessage));
    }
}
