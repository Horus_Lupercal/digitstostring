package com.GrandMA.NumberToString.exception;

public class MoneyFormatException extends RuntimeException {
    public MoneyFormatException(String value) {
        super(String.format("Something wrong with %s value, The value must have a follower format: XX.XX", value));
    }
}
