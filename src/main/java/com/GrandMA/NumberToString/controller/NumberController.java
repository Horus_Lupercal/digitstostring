package com.GrandMA.NumberToString.controller;

import com.GrandMA.NumberToString.exception.MoneyFormatException;
import com.GrandMA.NumberToString.service.NumberService;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NumberController {
    private NumberService service;
    private JLabel responseLabel;
    private JButton button;
    private JTextField textField;

    public NumberController(NumberService service) {
        this.service = service;
        button = new JButton("Enter");
        textField = new JTextField();
        responseLabel = new JLabel();
    }

    public void showEvent() {
        createEvent();
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                try {
                    String response = service.convert(textField.getText());
                    responseLabel.setText(response);
                } catch (MoneyFormatException e) {
                    responseLabel.setText(e.getMessage());
                }
            }
        });
    }

    private void createEvent() {
        JFrame mainFrame = new JFrame("Number Converter");
        button.setBounds(300, 100, 140, 40);

        JLabel textLabel = new JLabel();
        textLabel.setText("Enter digit :");
        textLabel.setBounds(225, 15, 100, 100);

        responseLabel.setBounds(10, 110, 800, 100);

        textField.setBounds(305, 50, 130, 30);

        mainFrame.add(responseLabel);
        mainFrame.add(textField);
        mainFrame.add(textLabel);
        mainFrame.add(button);
        mainFrame.setSize(800, 300);
        mainFrame.setLayout(null);
        mainFrame.setVisible(true);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
