package com.GrandMA.NumberToString;

import com.GrandMA.NumberToString.controller.NumberController;
import com.GrandMA.NumberToString.service.NumberService;

public class ApplicationClass {

    public static void main(String[] args) {
        NumberController numberController = new NumberController(new NumberService());
        numberController.showEvent();
    }
}
