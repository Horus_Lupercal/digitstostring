package com.GrandMA.NumberToString.service;

import com.GrandMA.NumberToString.exception.MoneyFormatException;

import java.text.DecimalFormat;

public class NumberService {
    private final String[] numbersArray = {
            "",
            " one",
            " two",
            " three",
            " four",
            " five",
            " six",
            " seven",
            " eight",
            " nine",
            " ten",
            " eleven",
            " twelve",
            " thirteen",
            " fourteen",
            " fifteen",
            " sixteen",
            " seventeen",
            " eighteen",
            " nineteen",
    };

    private final String[] tensArray = {
            "",
            " ten",
            " twenty",
            " thirty",
            " forty",
            " fifty",
            " sixty",
            " seventy",
            " eighty",
            " ninety"
    };


    public String convert(String numb) {
        if (numb == null || !numb.matches("^[0-9]*[.][0-9]{2}") || numb.isEmpty()) {
            throw new MoneyFormatException(numb);
        }
        if (numb.equals("0.00")) {
            return "zero dollar zero cent";
        }
        String[] test = numb.split("\\.");

        String dollarString = getDollars(test[0]).trim().replaceAll(" +", " ");
        String centString = getCents(test[1]).trim().replaceAll(" +", " ");

        return dollarString + " " + centString;
    }

    private String getDollars(String dollars) {
        final long dollarsDigitValue = Long.parseLong(dollars);
        if (dollarsDigitValue > 1_000_000_000) {
            return "The digit value should be less then 1 billion";
        }
        String mask = "000000000000";
        DecimalFormat decimalFormat = new DecimalFormat(mask);
        String stringValue = decimalFormat.format(dollarsDigitValue);
        StringBuilder result = new StringBuilder();

        String billions = stringValue.substring(0, 3);
        String millions = stringValue.substring(3, 6);
        String hundredThousands = stringValue.substring(6, 9);
        String thousands = stringValue.substring(9, 12);

        if (Integer.parseInt(billions) > 0) {
            result.append(convertLessThatThousand(Integer.parseInt(billions)));
            result.append(" billion");
        }

        if (Integer.parseInt(millions) > 0) {
            result.append(convertLessThatThousand(Integer.parseInt(millions)));
            result.append(" million");
        }

        if (Integer.parseInt(hundredThousands) > 0) {
            result.append(convertLessThatThousand(Integer.parseInt(hundredThousands)));
            result.append(" thousand");
        }

        if (Integer.parseInt(thousands) > 0) {
            result.append(convertLessThatThousand(Integer.parseInt(thousands)));
        }

        result.append(" dollar");

        if (dollarsDigitValue > 1) {
            result.append("s");
        }
        return result.toString();
    }

    private String convertLessThatThousand(int hundreds) {
        String ones = "";

        if (hundreds % 100 < 20) {
            ones = numbersArray[hundreds % 100];
            hundreds /= 100;
        } else {
            ones = numbersArray[hundreds % 10];
            hundreds /= 10;
            ones = tensArray[hundreds % 10] + ones;
            hundreds /= 10;
        }

        ones = ones.trim().replace(" ", "-");

        if (hundreds == 0) {
            return ones;
        }
        return numbersArray[hundreds] + " hundred" + " " + ones;
    }

    private String getCents(String cents) {
        if (cents.length() != 2) {
            throw new MoneyFormatException(cents);
        }
        final int centsDigitValue = Integer.parseInt(cents);

        StringBuilder result = new StringBuilder();
        result.append(convertLessThatThousand(centsDigitValue));
        if (result.toString().isEmpty()) {
            return "zero cent";
        } else {
            result.append(" cent");
        }
        if (centsDigitValue > 1) {
            result.append("s");
        }
        return result.toString();
    }
}
